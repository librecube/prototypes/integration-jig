# Integration Jig

A prototype of an integration jig for 1U - 3U CubeSats to allow for easy access to CubeSats during development and testing.

![](docs/vertical_jig.png)
